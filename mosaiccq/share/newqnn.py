import circuits
from convenience_qnn import nbrParamGates, nangles, nqubits, oneQubitGates
import torch
from pennylane import numpy as np
from pennylane import Tracker
import circuits
import pandas as pd

""" currently not used
def affine(inp, params, exhausted_params):
    w = model.params[exhausted_params:exhausted_params+len(inp)]
    exhausted_params += len(w)
    b = model.params[exhausted_params]
    exhausted_params += 1
    angle = torch.dot(w, inp) + b
    return angle, exhausted_params

def linear_w(inp, params, exhausted_params):
    w = model.params[exhausted_params:exhausted_params+len(inp)]
    exhausted_params += len(w)
    angle = torch.dot(w, inp) 
    return angle, exhausted_params

def linear_1(inp, params, exhausted_params):
    angle = torch.sum(inp)
    exhausted_params += 0
    return angle, exhausted_params
"""

def calcAnglesFull(inp, model):
    ''' 
    Function that works for all models and gates in which the data is uploaded as th=w.x+b
    '''
    dim = len(inp)
    angles = []

    exhausted_params = 0
    for i in range(model.nb_angles):
            w = model.params[exhausted_params:exhausted_params+dim]
            exhausted_params += len(w)
            b = model.params[exhausted_params]
            exhausted_params += 1
            angle = torch.dot(w, inp) + b
            angles.append(angle)
    assert(len(angles) == model.nb_angles)
    return angles

def calcAnglesAlt(inp, model):
    ''' 
    Function that works for all models and gates in which the data is uploaded as th=w.x or th=b in alternance, starting with th=w.x
    '''
    dim = len(inp)
    angles = []

    exhausted_params = 0
    for i in range(model.nb_angles):
        if i % 2 == 0:
            w = model.params[exhausted_params:exhausted_params+dim]
            angle = torch.dot(w, inp)
            exhausted_params += len(w)
        else:
            b = model.params[exhausted_params] 
            angle = b
            exhausted_params += 1
        angles.append(angle)
    assert(len(angles) == model.nb_angles)
    return angles

def calcAnglesFourier(inp, model):
    ''' 
    Function that works for all models and gates in which the data is uploaded as th=b (in a block) or th=x (end of a block)
    '''
    dim = len(inp)
    angles = []

    exhausted_params = 0
    for i in range(model.nb_angles):
        if (i+1) % model.block_length == 0:
            angle = torch.sum(inp)
        else:
            b = model.params[exhausted_params]
            exhausted_params += 1
            angle = torch.sum(inp) + b
        angles.append(angle)
    assert(len(angles) == model.nb_angles)
    return angles

def calcAnglesWeightedFourier(inp, model):
    ''' 
    Function that works for all models and gates in which the data is uploaded as th=b (in a block) or th=w.x (end of a block)
    '''
    dim = len(inp)
    angles = []

    exhausted_params = 0
    for i in range(model.nb_angles):
        if (i+1) % model.block_length == 0:
            w = model.params[exhausted_params:exhausted_params+dim]
            angle = torch.dot(w,inp)
            exhausted_params += len(w)
        else:
            b = model.params[exhausted_params]
            exhausted_params += 1
            angle = torch.sum(inp) + b
        angles.append(angle)
    assert(len(angles) == model.nb_angles)
    return angles

def calcAnglesFastFourier(inp, model):
    ''' 
    Function that works for all models and gates in which the data is uploaded as th=2^(-k)*1.x+b, i.e. k^th weight equal to 1/2^k
    FIXME: this is not up to date w.r.t the naming convention, see calcAnglesFourier to adapat this function
    '''
    dim = len(inp)
    angles = []

    exhausted_params = 0
    for i in range(model.nb_angles):
            b = model.params[exhausted_params]
            exhausted_params += 1
            angle = torch.sum(inp) / 2**i + b
            angles.append(angle)
    assert(len(angles) == model.nb_angles)
    return angles

def update_tracker(global_tracker, temp_tracker):
    '''
    This because the tracker must be declared in the forward() function, but is dumped right after. Using "with self.tracker as ..." does not work either. This function serves to go around these issues and have a global tracker that works outside of a given batch.
    '''
    if global_tracker.totals == {}:
        global_tracker.totals = temp_tracker.totals
        global_tracker.history = temp_tracker.history
    else: # non-empty global tracker
        for key in global_tracker.totals.keys():
            global_tracker.totals[key] += temp_tracker.totals[key]
        for key in global_tracker.history.keys():
            global_tracker.history[key] += temp_tracker.history[key]
    return 

def draw(model):
    '''
    Plot the circuit corresponding to the model. 
    '''
    import pennylane as qml
    import matplotlib.pyplot as plt
    angles = [0] * model.nb_angles
    encoding_angles = [0] * model.nb_qubits
    angles = "x" * model.nb_angles # this also works
    try:
        drawing = qml.draw(model.circuit)(model, angles, encoding_angles)
    except:
        drawing = qml.draw(model.circuit)(model, angles)

    # TODO: find the right place to store this
    f = open('drawing.out', 'w')
    f.write(drawing)
    f.close()
    return 1


# Parent class for one-qubit re-uploaders
class reuploading(torch.nn.Module):
    """
    Parent class for one-qubit re-uploaders
    """
    def __init__(self, circuit_name, block, nb_qubits, depth, input_size):
        super(reuploading, self).__init__() # inherit from torch.nn.Module

        assert(all([gate in oneQubitGates for gate in block.split("_")]))
        # Circuit-related parameters
        self.name = circuit_name
        self.circuit   = circuits.get_circuit(circuit_name)
        self.block = block.split("_")
        self.block_length = len(self.block)
        self.input_size = int(input_size)
        self.nb_qubits = int(nb_qubits)
        self.depth = int(depth) # (curr) nbr of reps of the block
        self.seq   = (self.block * self.depth) #e.g. ['RY', 'RX']
        self.nb_gates = len(self.seq) 
        # Angles
        self.list_nb_angles = self.calc_list_nb_angles()
        self.nb_angles = np.sum(self.list_nb_angles)
        # Tracker of the circuit evaluations
        self.tracker = Tracker(self.circuit.device)
        # Pretty drawing
        draw(self)

    # Functions to assign gate <- nb angles <- nb params
    # TODO: replace by PennyLane rotations attributes
    def calc_nb_angles(self, gate_name):
        if gate_name in nbrParamGates.keys(): return nbrParamGates[gate_name]
        else: raise ValueError('Gate name %s not recognised' %(gate_name))

    def calc_list_nb_angles(self):
        return [self.calc_nb_angles(gate) for gate in self.seq]
        
    def save_model(self, path):
        path += '.csv'
        P = [p.detach() for p in self.params]
        dfParams = pd.DataFrame({'Params': P})
        dfParams.to_csv(path, index=False)

    def load_model(self,path):
        pass

class reuploadingFull(reuploading):
    '''
    Class for the full reuploading, in which the angles are th=w.x+b
    '''
    def __init__(self, circuit_name, block, nb_qubits, depth, input_size):
        super(reuploadingFull, self).__init__(circuit_name, block, nb_qubits, depth, input_size) # inherit from parent class

        # Parameters
        self.list_nb_params = self.calc_list_nb_params() # nbr of params for the gates
        self.nb_params = np.sum(self.list_nb_params)

        # Parameters initialisation and rescaling
        self.params = torch.rand(self.nb_params, requires_grad=True)
        self.params = self.params.add(-.5).clone().detach().requires_grad_(True) #preferred syntax, cf the docs
        self.params = self.params.multiply(2*torch.pi).clone().detach().requires_grad_(True)

    # forward func mimicking the MLP one
    def forward(self, train_input, run_infos, module_infos):
        preds = torch.empty((len(train_input)))
        i = 0 #index
        with Tracker(self.circuit.device) as tracker:
            for inp in train_input:
                angles = calcAnglesFull(inp, self)
                val_pred = self.circuit(self, angles)
                preds[i] = val_pred
                i += 1
        update_tracker(self.tracker, tracker)
        print(self.tracker.totals)
        return preds

    def calc_list_nb_params(self):
        return [(self.input_size + 1) * self.calc_nb_angles(gate) for gate in self.seq]

    def parameters(self):
        return [self.params]

    def info(self):
        return  { "nb_params" : self.nb_params,
                "nb_gates" : self.nb_gates,
                "tracker totals": self.tracker.totals,
                "tracker history": self.tracker.history,
                "tracker latest": self.tracker.latest}


class reuploadingAlt(reuploading):
    '''
    Class for the alternate reuploading, in which the angles are alternatively th=w.x and th=b
    '''
    def __init__(self, circuit_name, block, nb_qubits, depth, input_size):
        super(reuploadingAlt, self).__init__(circuit_name, block, nb_qubits, depth, input_size) # inherit from parent class

        # Parameters
        self.list_nb_params = self.calc_list_nb_params() # nbr of params for the gates
        self.nb_params = np.sum(self.list_nb_params)

        # Parameters initialisation and rescaling
        self.params = torch.rand(self.nb_params, requires_grad=True)
        self.params = self.params.add(-.5).clone().detach().requires_grad_(True) #preferred syntax, cf the docs
        self.params = self.params.multiply(2*torch.pi).clone().detach().requires_grad_(True)

    # forward func mimicking the MLP one
    def forward(self, train_input, run_infos, module_infos):
        preds = torch.empty((len(train_input)))
        i = 0 #index
        with Tracker(self.circuit.device) as tracker:
            for inp in train_input:
                angles = calcAnglesAlt(inp, self)
                val_pred = self.circuit(self, angles)
                preds[i] = val_pred
                i += 1
        update_tracker(self.tracker, tracker)
        print(self.tracker.totals)
        return preds

    def calc_list_nb_params(self):
        res = []
        for i in range(len(self.seq)):
            gate = self.seq[i]
            # if i is even, the angle th_i = w_i . x
            if i % 2 == 0:
                nb = self.input_size * self.calc_nb_angles(gate)
            # if i is odd, the angle th_i = b_i
            else:
                nb = 1
            res.append(nb)
        return res 

    def parameters(self):
        return [self.params]

    def info(self):
        return  { "nb_params" : self.nb_params,
                "nb_gates" : self.nb_gates,
                "tracker totals": self.tracker.totals,
                "tracker history": self.tracker.history,
                "tracker latest": self.tracker.latest}

class reuploadingFourier(reuploading):
    '''
    Class for the Fourier reuploading, in which the angles are th=1.x (first rotation of a block) or th=b (all the others)
    '''
    def __init__(self, circuit_name, block, nb_qubits, depth, input_size):
        super(reuploadingFourier, self).__init__(circuit_name, block, nb_qubits, depth, input_size) # inherit from parent class

        # Parameters
        self.list_nb_params = self.calc_list_nb_params() # nbr of params for the gates
        self.nb_params = np.sum(self.list_nb_params)

        # Parameters initialisation and rescaling
        self.params = torch.rand(self.nb_params, requires_grad=True)
        self.params = self.params.add(-.5).clone().detach().requires_grad_(True) #preferred syntax, cf the docs
        self.params = self.params.multiply(2*torch.pi).clone().detach().requires_grad_(True)

    # forward func mimicking the MLP one
    def forward(self, train_input, run_infos, module_infos):
        preds = torch.empty((len(train_input)))
        i = 0 #index
        with Tracker(self.circuit.device) as tracker:
            for inp in train_input:
                angles = calcAnglesFourier(inp, self)
                val_pred = self.circuit(self, angles)
                preds[i] = val_pred
                i += 1
        update_tracker(self.tracker, tracker)
        print(self.tracker.totals)
        return preds

    def calc_list_nb_params(self):
        nb = []
        for i in range(len(self.seq)):
            if (i+1) % self.block_length != 0:
                gate = self.seq[i]
                nb.append(self.calc_nb_angles(gate))
        return nb

    def parameters(self):
        return [self.params]

    def info(self):
        return  { "nb_params" : self.nb_params,
                "nb_gates" : self.nb_gates,
                "tracker totals": self.tracker.totals,
                "tracker history": self.tracker.history,
                "tracker latest": self.tracker.latest}

class reuploadingWeightedFourier(reuploading):
    '''
    Class for the Fourier reuploading, in which the angles are th=1.x (first rotation of a block) or th=w.b (all the others)
    '''
    def __init__(self, circuit_name, block, nb_qubits, depth, input_size):
        super(reuploadingWeightedFourier, self).__init__(circuit_name, block, nb_qubits, depth, input_size) # inherit from parent class

        # Parameters
        self.list_nb_params = self.calc_list_nb_params() # nbr of params for the gates
        self.nb_params = np.sum(self.list_nb_params)

        # Parameters initialisation and rescaling
        self.params = torch.rand(self.nb_params, requires_grad=True)
        self.params = self.params.add(-.5).clone().detach().requires_grad_(True) #preferred syntax, cf the docs
        self.params = self.params.multiply(2*torch.pi).clone().detach().requires_grad_(True)

    # forward func mimicking the MLP one
    def forward(self, train_input, run_infos, module_infos):
        preds = torch.empty((len(train_input)))
        i = 0 #index
        with Tracker(self.circuit.device) as tracker:
            for inp in train_input:
                angles = calcAnglesWeightedFourier(inp, self)
                val_pred = self.circuit(self, angles)
                preds[i] = val_pred
                i += 1
        update_tracker(self.tracker, tracker)
        print(self.tracker.totals)
        return preds

    def calc_list_nb_params(self):
        nb = []
        for i in range(len(self.seq)):
            if (i+1) % self.block_length != 0:
                gate = self.seq[i]
                nb.append(self.calc_nb_angles(gate))
            else:
                gate = self.seq[i]
                nb.append(self.input_size * self.calc_nb_angles(gate))
        return nb

    def parameters(self):
        return [self.params]

    def info(self):
        return  { "nb_params" : self.nb_params,
                "nb_gates" : self.nb_gates,
                "tracker totals": self.tracker.totals,
                "tracker history": self.tracker.history,
                "tracker latest": self.tracker.latest}

class reuploadingFastFourier(torch.nn.Module):
    '''
    Class for the Fourier reuploading, in which the angles are th=1.x+b
    CAVEAT: makes sense only for 1d data?
    '''
    def __init__(self, circuit_name, block, nb_qubits, depth, input_size):
        super(reuploadingFastFourier, self).__init__() # inherit from torch.nn.Module

        # Check(s) 
        assert(all([gate in oneQubitGates for gate in block.split("_")]))

        # Circuit-related parameters
        self.name = circuit_name
        self.circuit   = circuits.get_circuit(circuit_name)
        self.block = block.split("_")
        self.input_size = int(input_size)
        self.nb_qubits = int(nb_qubits)
        self.depth = int(depth)
        #self.seq       = (self.block * self.depth)[:self.depth] #e.g. ['RY', 'RX'] <= define 1 depth as one gate 
        self.seq       = (self.block * self.depth) #e.g. ['RY', 'RX'] # <= define 1 depth as one block

        # Tracker of the circuit evaluations
        self.tracker = Tracker(self.circuit.device)
        
        # Angles and parameters
        self.list_nb_params = self.calc_list_nb_params() # nbr of params for the gates
        self.nb_params = np.sum(self.list_nb_params) #FIXME: valid only if all rotations upload the data
        self.list_nb_angles = self.calc_list_nb_angles()
        self.nb_angles = np.sum(self.list_nb_angles)

        # Parameters initialisation and rescaling
        self.params = torch.rand(self.nb_params, requires_grad=True)
        self.params = self.params.add(-.5).clone().detach().requires_grad_(True) #preferred syntax, cf the docs
        self.params = self.params.multiply(2*torch.pi).clone().detach().requires_grad_(True) 
   
        # Pretty drawing
        draw(self)

    def parameters(self):
        return [self.params]

    # forward func mimicking the MLP one
    def forward(self, train_input, run_infos, module_infos):
        preds = torch.empty((len(train_input))) 
        i = 0 #index
        with Tracker(self.circuit.device) as tracker:
            for inp in train_input:
                angles = calcAnglesFourier(inp, self)
                val_pred = self.circuit(self, angles)
                preds[i] = val_pred
                i += 1
        update_tracker(self.tracker, tracker)
        return preds

    def info(self):
        return  { "nb_params" : self.nb_params,
                "nb_gates" : self.nb_gates,
                "tracker totals": self.tracker.totals,
                "tracker history": self.tracker.history,
                "tracker latest": self.tracker.latest}

    # Functions to assign gate <- nb angles <- nb params
    # TODO: replace by PennyLane rotations attributes
    def calc_nb_angles(self, gate_name):
        if gate_name in nbrParamGates.keys(): return nbrParamGates[gate_name]
        else: raise ValueError('Gate name %s not recognised' %(gate_name))

    def calc_list_nb_angles(self):
        return [self.calc_nb_angles(gate) for gate in self.seq]
        
    def calc_list_nb_params(self): # 1 bias per angle, no variational params for weights
        return [self.calc_nb_angles(gate) for gate in self.seq]
    

    def save_model(self, path):
        path += '.csv'
        P = [p.detach() for p in self.params]
        dfParams = pd.DataFrame({'Params': P})
        dfParams.to_csv(path, index=False)

    def load_model(self,path):
        pass


class reuploadingZYZ(torch.nn.Module):
    '''
    Class for the full reuploading, in which the angles are
    parametrised as in https://arxiv.org/abs/2205.07848, i.e.
    one block is RZ(phi)RY(th)RZ(x)
    '''
    def __init__(self, circuit_name, block, nb_qubits, depth, input_size):
        super(reuploadingZYZ, self).__init__() # inherit from torch.nn.Module

        # Check(s) 
        assert(all([gate in oneQubitGates for gate in block.split("_")]))

        # Circuit-related parameters
        self.name = circuit_name
        self.circuit   = circuits.get_circuit(circuit_name)
        self.block = block.split("_")
        self.block_length = len(self.block)
        self.input_size = int(input_size)
        self.nb_qubits = int(nb_qubits)
        self.depth = int(depth)
        #self.seq       = (self.block * self.depth)[:self.depth] #e.g. ['RY', 'RX'] <= define 1 depth as one gate 
        self.seq       = (self.block * self.depth) #e.g. ['RY', 'RX'] # <= define 1 depth as one block

        # Tracker of the circuit evaluations
        self.tracker = Tracker(self.circuit.device)

        # Angles and parameters
        self.list_nb_params = self.calc_list_nb_params() # nbr of params for the gates
        self.nb_params = np.sum(self.list_nb_params)
        self.list_nb_angles = self.calc_list_nb_angles()
        self.nb_angles = np.sum(self.list_nb_angles)

        # Parameters initialisation and rescaling
        self.params = torch.rand(self.nb_params, requires_grad=True)
        self.params = self.params.add(-.5).clone().detach().requires_grad_(True) #preferred syntax, cf the docs
        self.params = self.params.multiply(2*torch.pi).clone().detach().requires_grad_(True) 
        
        # Pretty drawing
        draw(self)

    def parameters(self):
        return [self.params]

    # forward func mimicking the MLP one
    def forward(self, train_input, run_infos, module_infos):
        preds = torch.empty((len(train_input))) 
        i = 0 #index
        #with Tracker(self.circuit.device, callback=self.update_tracker) as tracker:
        with Tracker(self.circuit.device) as tracker:
            #with self.tracker as tracker:
            for inp in train_input:
                #angles = calcAnglesZYZ(inp, self)
                angles = calcAngleFourier(inp, self)
                val_pred = self.circuit(self, angles)
                preds[i] = val_pred
                i += 1
        update_tracker(self.tracker, tracker)
        print(self.tracker.totals)
        return preds

    def info(self):
        return  { "nb_params" : self.nb_params,
                "tracker totals": self.tracker.totals,
                "tracker history": self.tracker.history,
                "tracker latest": self.tracker.latest}

    # Functions to assign gate <- nb angles <- nb params
    # TODO: replace by PennyLane rotations attributes
    def calc_nb_angles(self, gate_name):
        if gate_name in nbrParamGates.keys(): return nbrParamGates[gate_name]
        else: raise ValueError('Gate name %s not recognised' %(gate_name))

    def calc_list_nb_angles(self):
        return [self.calc_nb_angles(gate) for gate in self.seq]
        
    def calc_list_nb_params(self):
        nb = []
        for i in range(len(self.seq)):
            if (i+1) % self.block_length != 0:
                gate = self.seq[i]
                nb.append(self.calc_nb_angles(gate))
        return nb
    
    def save_model(self, path):
        path += '.csv'
        P = [p.detach() for p in self.params]
        dfParams = pd.DataFrame({'Params': P})
        dfParams.to_csv(path, index=False)

    def load_model(self,path):
        pass



# Classes for TTN and MERA. TODO: one of them (first one?) can be removed
class ttn_and_mera_linear(torch.nn.Module):
    
    def __init__(self, circuit_name, block, input_size):
        super(ttn_and_mera_linear, self).__init__() # inherit from torch.nn.Module

        # Circuit-related parameters
        self.name = "TTN"
        self.circuit = circuits.get_circuit(self.name)
        self.block = block.split("_")
        self.downscaling = 2 # FIXME
        self.input_size = int(input_size)
        self.nb_qubits = self.input_size
        self.calc_attributes(self.nb_qubits, self.downscaling, self.block)

        # Tracker of the circuit evaluations
        self.tracker = Tracker(self.circuit.device)
        
        # Parameters initialisation and rescaling
        self.params = torch.rand(self.nb_params, requires_grad=True)
        self.params = self.params.add(-.5).clone().detach().requires_grad_(True) #preferred syntax, cf the docs
        self.params = self.params.multiply(2*torch.pi).clone().detach().requires_grad_(True) 
        
        # Pretty drawing
        draw(self)
   
    def parameters(self):
        return [self.params]

    # forward func mimicking the MLP one
    def forward(self, train_input, run_infos, module_infos):
        preds = torch.empty((len(train_input))) 
        i = 0 #index
        with Tracker(self.circuit.device) as tracker:
            for inp in train_input:
                encoding_angles = inp
                val_pred = self.circuit(self, self.params, encoding_angles)
                preds[i] = val_pred
                i += 1
        update_tracker(self.tracker, tracker)
        return preds
    
    def calc_attributes(self, nq, s, seq):
        ### Calculation of the circuit's size and relevant quantities

        # depths
        depth_s = int(np.log(nq) / np.log(s)) # number of layers with s-qubits gates
        depth_tot = int(depth_s * len(seq))   # total number of layers

        # qubit counts of each gate
        nqs = [nqubits[gate] for gate in seq]

        # angles counts of each gate
        nas = [nangles[gate] for gate in seq]

        # qubit counts of the different gates in the block
        one_q = np.sum([1 for rot in seq if nqubits[rot] == 1]) # nbr of one qubits gates

        # count the number of gates according to the number of qubits they act on
        n_s = int((s**depth_s - 1) / (s-1))
        n_1 = int(n_s * s) # number of occurences of each single-qubit gate in seq
        n_1tot = n_1 * one_q # total number of one qubit gates

        # angles count
        a_1 = 0 # angles for 1-qubit gates
        a_s = 0 # angles for s-qubits gates
        for gate in seq:
            if nqubits[gate] == 1:
                a_1 += nangles[gate]
            if nqubits[gate] == s:
                a_s += nangles[gate]
        a_1 *= int(n_1)
        a_s *= int(n_s)

        # angles and params count
        na = [nangles[gate] for gate in seq]
        oc = [] # number of occurrences of each gate
        for gate in seq:
            if nqubits[gate] == 1:
                oc.append(n_1)
            if nqubits[gate] == 2:
                oc.append(n_s)
        print('na ', na)
        print('oc ', oc)
        nb_angles = np.sum([ang * occ for ang, occ in zip(na, oc)])
        print(nas)
        nb_params = np.sum([ang * occ for ang, occ in zip(na, oc)]) # the multiplicative factor depends on the form of the data uploading
        print('nb_params ', nb_params)
        newseq = seq * depth_s
        print(newseq)

        self.nb_params = nb_params 
        self.nb_angles = nb_angles
        self.seq = newseq
    
    # Functions to assign gate <- nb angles <- nb params
    # TODO: replace by PennyLane rotations attributes
    def calc_nb_angles(self, gate_name):
        if gate_name in nbrParamGates.keys(): return nbrParamGates[gate_name]
        else: raise ValueError('Gate name %s not recognised' %s)

    def calc_list_nb_angles(self):
        return [self.calc_nb_angles(gate) for gate in self.seq]
        
    def calc_list_nb_params(self): #TODO: this should be able to consider the reuploading scheme, e.g. R(wx+b)R(phi)
        return [(self.input_size + 1) * self.calc_nb_angles(gate) for gate in self.seq]
    
    def gen_seq(self):
        seq = []
        for n in range(self.nb_gates):
            seq += self.block
        return seq[:self.nb_gates] #creates paths containing spaces in save.py

    def nb_gates(self):
        if self.name == 'RUC': return self.depth
        elif self.name == 'TTN2': return 2**self.depth - 1

    def info(self):
        return  { "nb_params" : self.nb_params,
                "tracker totals": self.tracker.totals,
                "tracker history": self.tracker.history,
                "tracker latest": self.tracker.latest}

    def save_model(self, path):
        path += '.csv'
        P = [p.detach() for p in self.params]
        dfParams = pd.DataFrame({'Params': P})
        dfParams.to_csv(path, index=False)

    def load_model(self,path):
        pass

class ttn_and_mera(torch.nn.Module):
    
    def __init__(self, circuit_name, block, input_size, downscaling):
        super(ttn_and_mera, self).__init__() # inherit from torch.nn.Module

        # Circuit-related parameters
        self.name = circuit_name
        self.circuit = circuits.get_circuit(self.name)
        self.block = block.split("_")
        self.downscaling = int(downscaling)
        self.input_size = int(input_size)
        self.nb_qubits = self.input_size #FIXME: must be able to give custom nb_qubits
        self.calc_attributes(self.nb_qubits, self.downscaling, self.block)
        
        # Tracker of the circuit evaluations
        self.tracker = Tracker(self.circuit.device)

        # Parameters initialisation and rescaling
        self.params = torch.rand(self.nb_params, requires_grad=True)
        self.params = self.params.add(-.5).clone().detach().requires_grad_(True) #preferred syntax, cf the docs
        self.params = self.params.multiply(2*torch.pi).clone().detach().requires_grad_(True)
   
        # Pretty drawing
        draw(self)

    def parameters(self):
        return [self.params]

    # forward func mimicking the MLP one
    def forward(self, train_input, run_infos, module_infos):
        preds = torch.empty((len(train_input))) 
        i = 0 #index
        with Tracker(self.circuit.device) as tracker:
            for inp in train_input:
                encoding_angles = [0] * len(inp)#np.arcsin(inp)
                angles = self.calcAngles(inp)
                angles = calcAnglesRuc(inp, self)

                val_pred = self.circuit(self, angles, encoding_angles)
                preds[i] = val_pred
                i += 1
        update_tracker(self.tracker, tracker)
        return preds
    
    def calc_attributes(self, nq, s, seq):
        ### Calculation of the circuit's size and relevant quantities

        # depths
        depth_s = int(np.log(nq) / np.log(s)) # number of layers with s-qubits gates
        depth_tot = int(depth_s * len(seq))   # total number of layers

        # qubit counts of each gate
        nqs = [nqubits[gate] for gate in seq]

        # angles counts of each gate
        nas = [nangles[gate] for gate in seq]

        # qubit counts of the different gates in the block
        one_q = np.sum([1 for rot in seq if nqubits[rot] == 1]) # nbr of one qubits gates

        # count the number of gates according to the number of qubits they act on
        n_s = int((s**depth_s - 1) / (s-1))
        n_1 = int(n_s * s) # number of occurences of each single-qubit gate in seq
        n_1tot = n_1 * one_q # total number of one qubit gates

        # angles count
        a_1 = 0 # angles for 1-qubit gates
        a_s = 0 # angles for s-qubits gates
        for gate in seq:
            if nqubits[gate] == 1:
                a_1 += nangles[gate]
            if nqubits[gate] == s:
                a_s += nangles[gate]
        a_1 *= int(n_1)
        a_s *= int(n_s)

        # angles and params count
        ocs = [] # number of occurrences of each gate
        for gate in seq:
            if nqubits[gate] == 1:
                ocs.append(n_1)
            if nqubits[gate] == 2:
                ocs.append(n_s)
        print('nas ', nas)
        print('ocs ', ocs)
        nb_angles = np.sum([ang * occ for ang, occ in zip(nas, ocs)])
        nb_params = (self.input_size + 1) * np.sum([ang * occ for ang, occ in zip(nas, ocs)]) # the multiplicative factor depends on the form of the data uploading
        newseq = seq * depth_s
        print(newseq)

        self.nb_angles = nb_angles
        self.nb_params = nb_params 
        print('nb_params ', nb_params)
        self.seq = newseq
        self.occ = ocs
    
    # Function computing the angles for a given data point
    def calcAngles(self, x):
        dim = self.input_size
        angles = []
        for i in range(self.nb_angles):
            w = self.params[i*dim:(i+1)*dim]
            b = self.params[(i+1)*dim]
            angle = torch.dot(w, x) + b
            angles.append(angle)
        return angles

    # Functions to assign gate <- nb angles <- nb params
    # TODO: replace by PennyLane rotations attributes
    def calc_nb_angles(self, gate_name):
        if gate_name in nbrParamGates.keys(): return nbrParamGates[gate_name]
        else: raise ValueError('Gate name %s not recognised' %s)

    def calc_list_nb_angles(self):
        return [self.calc_nb_angles(gate) for gate in self.seq]
        
    def calc_list_nb_params(self): #TODO: this should be able to consider the reuploading scheme, e.g. R(wx+b)R(phi)
        return [(self.input_size + 1) * self.calc_nb_angles(gate) for gate in self.seq]
    
    def gen_seq(self):
        seq = []
        for n in range(self.nb_gates):
            seq += self.block
        return seq[:self.nb_gates] #creates paths containing spaces in save.py

    def nb_gates(self):
        if self.name == 'RUC': return self.depth
        elif self.name == 'TTN2': return 2**self.depth - 1

    def info(self):
        return  { "nb_params" : self.nb_params,
                "tracker totals": self.tracker.totals,
                "tracker history": self.tracker.history,
                "tracker latest": self.tracker.latest}

    def save_model(self, path):
        path += '.csv'
        P = [p.detach() for p in self.params]
        dfParams = pd.DataFrame({'Params': P})
        dfParams.to_csv(path, index=False)

    def load_model(self,path):
        pass
