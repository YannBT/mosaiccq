import circuits
from convenience_qnn import nbrParamGates, nangles, nqubits, oneQubitGates
import torch
from pennylane import numpy as np
from pennylane import Tracker
import circuits
import pandas as pd

def calcAnglesRuc(inp, model):
    ''' 
    Function that works for all models and gates in which the data is uploaded as th=w.x+b
    '''
    dim = len(inp)
    angles = []

    exhausted_params = 0
    for i in range(model.nb_angles):
            w = model.params[exhausted_params:exhausted_params+dim]
            exhausted_params += len(w)
            b = model.params[exhausted_params]
            exhausted_params += 1
            angle = torch.dot(w, inp) + b
            angles.append(angle)
    assert(len(angles) == model.nb_angles)
    return angles

def calcAnglesAlt(inp, model):
    ''' 
    Function that works for all models and gates in which the data is uploaded as th=w.x or th=b in alternance, starting with th=w.x
    '''
    dim = len(inp)
    angles = []

    exhausted_params = 0
    for i in range(model.nb_angles):
        if i % 2 == 0:
            w = model.params[exhausted_params:exhausted_params+dim]
            angle = torch.dot(w, inp)
            exhausted_params += len(w)
        else:
            b = model.params[exhausted_params] 
            angle = b
            exhausted_params += 1
        print('nbr exhausted params ', exhausted_params)
        angles.append(angle)
    assert(len(angles) == model.nb_angles)
    return angles

def calcAnglesFourier(inp, model):
    ''' 
    Function that works for all models and gates in which the data is uploaded as th=b (in a block) or th=x (end of a block)
    '''
    dim = len(inp)
    angles = []

    exhausted_params = 0
    for i in range(model.nb_angles):
        if (i+1) % model.block_length == 0:
            angle = torch.sum(inp)
        else:
            b = model.params[exhausted_params]
            exhausted_params += 1
            angle = torch.sum(inp) + b
        angles.append(angle)
    assert(len(angles) == model.nb_angles)
    return angles

def calcAnglesFastFourier(inp, model):
    ''' 
    Function that works for all models and gates in which the data is uploaded as th=2^(-k)*1.x+b, i.e. k^th weight equal to 1/2^k
    FIXME: this is not up to date w.r.t the naming convention, see calcAnglesFourier to adapat this function
    '''
    dim = len(inp)
    angles = []

    exhausted_params = 0
    for i in range(model.nb_angles):
            b = model.params[exhausted_params]
            exhausted_params += 1
            angle = torch.sum(inp) / 2**i + b
            angles.append(angle)
    assert(len(angles) == model.nb_angles)
    return angles

#def calcAnglesZYZ(inp, model):
# NOT USED anymore: now a special case of Fourier cf a convention
#    ''' 
#    Function that works for models where the elementary block is RZ(phi)RY(th)RZ(x)
#    '''
#    dim = len(inp)
#    assert(dim == 1)
#    angles = []
#
#    exhausted_params = 0
#    for i in range(model.nb_angles):
#        if (i+1) % model.block_length == 0: # upload x
#            angles.append(inp)
#        else: # trainable angle
#            angles.append(model.params[exhausted_params])
#            exhausted_params += 1
#    assert(len(angles) == model.nb_angles)
#    return angles

def update_tracker(global_tracker, temp_tracker):
    '''
    This because the tracker must be declared in the forward() function, but is dumped right after. Using "with self.tracker as ..." does not work either. This function serves to go around this issues and have a global tracker that works outside of a given batch.
    '''
    if global_tracker.totals == {}:
        global_tracker.totals = temp_tracker.totals
        global_tracker.history = temp_tracker.history
    else:
        for key in global_tracker.totals.keys(): # non-empty global tracker
            global_tracker.totals[key] += temp_tracker.totals[key]
        for key in global_tracker.history.keys():
            global_tracker.history[key] += temp_tracker.history[key]
    return 

def draw(model):
    '''
    Plot the circuit corresponding to the model. 
    '''
    import pennylane as qml
    import matplotlib.pyplot as plt
    angles = [0] * model.nb_angles
    encoding_angles = [0] * model.nb_qubits
    angles = "x" * model.nb_angles # this also works
    try:
        drawing = qml.draw(model.circuit)(model, angles, encoding_angles)
    except:
        drawing = qml.draw(model.circuit)(model, angles)

    # TODO: find the right place to store this
    f = open('drawing.out', 'w')
    f.write(drawing)
    f.close()
    return 1


# Classes for one-qubit re-uploaders
class reuploading(torch.nn.Module):
    '''
    Class for the full reuploading, in which the angles are th=w.x+b
    '''
    def __init__(self, circuit_name, block, nb_qubits, depth, input_size):
        super(reuploading, self).__init__() # inherit from torch.nn.Module

        # Check(s) 
        assert(all([gate in oneQubitGates for gate in block.split("_")]))

        # Circuit-related parameters
        self.name = circuit_name
        self.circuit   = circuits.get_circuit(circuit_name)
        self.block = block.split("_")
        self.input_size = int(input_size)
        self.nb_qubits = int(nb_qubits)
        self.depth = int(depth)
        #self.seq       = (self.block * self.depth)[:self.depth] #e.g. ['RY', 'RX'] <= define 1 depth as one gate 
        self.seq       = (self.block * self.depth) #e.g. ['RY', 'RX'] # <= define 1 depth as one block

        # Tracker of the circuit evaluations
        self.tracker = Tracker(self.circuit.device)

        # Angles and parameters
        self.list_nb_params = self.calc_list_nb_params() # nbr of params for the gates
        self.nb_params = np.sum(self.list_nb_params)
        self.list_nb_angles = self.calc_list_nb_angles()
        self.nb_angles = np.sum(self.list_nb_angles)

        # Parameters initialisation and rescaling
        self.params = torch.rand(self.nb_params, requires_grad=True)
        self.params = self.params.add(-.5).clone().detach().requires_grad_(True) #preferred syntax, cf the docs
        self.params = self.params.multiply(2*torch.pi).clone().detach().requires_grad_(True) 
        
        """ for chip run ch1 """
        #self.params = torch.tensor(
        #    [-0.1983, 1.8851, 3.8224, -0.7547, -2.1438, 0.1556,
        #    -0.6154, 3.2644, 2.4182, -2.8632, 1.5269, 0.0544,
        #    -2.0349, -1.3905, -2.6264, 2.0487, 1.2086, 1.4315], requires_grad=True)

        """ for chip run ackley2 
        self.params = torch.tensor(
            [2.0538, 1.3714, 3.1489, 0.0515, 0.0454, -3.7305,
            2.1674, 1.8726, -1.5868, 0.9707, -0.9025, -3.1331,
            1.0943, -1.6765, -3.1480, 0.5445, -1.0203, 1.5846,
            0.8228, -0.8274, -0.1550, 2.8815, 0.7405, 2.8491],
            requires_grad=True) # run 1 in .runs_chip_ackley2
        """
    

        # Pretty drawing
        draw(self)

    def parameters(self):
        return [self.params]

    # forward func mimicking the MLP one
    def forward(self, train_input, run_infos, module_infos):
        preds = torch.empty((len(train_input))) 
        i = 0 #index
        #with Tracker(self.circuit.device, callback=self.update_tracker) as tracker:
        with Tracker(self.circuit.device) as tracker:
            #with self.tracker as tracker:
            for inp in train_input:
                angles = calcAnglesRuc(inp, self)
                val_pred = self.circuit(self, angles)
                preds[i] = val_pred
                i += 1
        update_tracker(self.tracker, tracker)
        print(self.tracker.totals)
        return preds

    def info(self):
        return  { "nb_params" : self.nb_params,
                "tracker totals": self.tracker.totals,
                "tracker history": self.tracker.history,
                "tracker latest": self.tracker.latest}

    # Functions to assign gate <- nb angles <- nb params
    # TODO: replace by PennyLane rotations attributes
    def calc_nb_angles(self, gate_name):
        if gate_name in nbrParamGates.keys(): return nbrParamGates[gate_name]
        else: raise ValueError('Gate name %s not recognised' %(gate_name))

    def calc_list_nb_angles(self):
        return [self.calc_nb_angles(gate) for gate in self.seq]
        
    def calc_list_nb_params(self): #TODO: this should be able to consider the reuploading scheme, e.g. R(wx+b)R(phi)
        return [(self.input_size + 1) * self.calc_nb_angles(gate) for gate in self.seq]
    
    def save_model(self, path):
        path += '.csv'
        P = [p.detach() for p in self.params]
        dfParams = pd.DataFrame({'Params': P})
        dfParams.to_csv(path, index=False)

    def load_model(self,path):
        pass


class reuploadingAlt(torch.nn.Module):
    '''
    Class for the alternate reuploading, in which the angles are alternatively th=w.x and th=b
    '''
    def __init__(self, circuit_name, block, nb_qubits, depth, input_size):
        super(reuploadingAlt, self).__init__() # inherit from torch.nn.Module

        # Check(s) 
        assert(all([gate in oneQubitGates for gate in block.split("_")]))

        # Circuit-related parameters
        self.name = circuit_name
        self.circuit   = circuits.get_circuit(circuit_name)
        self.block = block.split("_")
        self.input_size = int(input_size)
        self.nb_qubits = int(nb_qubits)
        self.depth = int(depth)
        #self.seq       = (self.block * self.depth)[:self.depth] #e.g. ['RY', 'RX'] <= define 1 depth as one gate 
        self.seq       = (self.block * self.depth) #e.g. ['RY', 'RX'] # <= define 1 depth as one block

        # Tracker of the circuit evaluations
        self.tracker = Tracker(self.circuit.device)
        
        # Angles and parameters
        self.list_nb_params = self.calc_list_nb_params() # nbr of params for the gates
        self.nb_params = np.sum(self.list_nb_params)
        self.list_nb_angles = self.calc_list_nb_angles()
        self.nb_angles = np.sum(self.list_nb_angles)

        # Parameters initialisation and rescaling
        self.params = torch.rand(self.nb_params, requires_grad=True)
        self.params = self.params.add(-.5).clone().detach().requires_grad_(True) #preferred syntax, cf the docs
        self.params = self.params.multiply(2*torch.pi).clone().detach().requires_grad_(True) 
   
        # Pretty drawing
        draw(self)

    def parameters(self):
        return [self.params]

    # forward func mimicking the MLP one
    def forward(self, train_input, run_infos, module_infos):
        preds = torch.empty((len(train_input))) 
        i = 0 #index
        with Tracker(self.circuit.device) as tracker:
            for inp in train_input:
                angles = calcAnglesAlt(inp, self)
                val_pred = self.circuit(self, angles)
                preds[i] = val_pred
                i += 1
        update_tracker(self.tracker, tracker)
        return preds

    def info(self):
        return  { "nb_params" : self.nb_params,
                "tracker totals": self.tracker.totals,
                "tracker history": self.tracker.history,
                "tracker latest": self.tracker.latest}

    # Functions to assign gate <- nb angles <- nb params
    # TODO: replace by PennyLane rotations attributes
    def calc_nb_angles(self, gate_name):
        if gate_name in nbrParamGates.keys(): return nbrParamGates[gate_name]
        else: raise ValueError('Gate name %s not recognised' %(gate_name))

    def calc_list_nb_angles(self):
        return [self.calc_nb_angles(gate) for gate in self.seq]
        
    def calc_list_nb_params(self):
        res = []
        for i in range(len(self.seq)):
            gate = self.seq[i]
            # if i is even, the angle th_i = w_i . x
            if i % 2 == 0:
                nb = self.input_size * self.calc_nb_angles(gate)
            # if i is odd, the angle th_i = b_i
            else:
                nb = 1
            res.append(nb)
        return res 

    def save_model(self, path):
        path += '.csv'
        P = [p.detach() for p in self.params]
        dfParams = pd.DataFrame({'Params': P})
        dfParams.to_csv(path, index=False)

    def load_model(self,path):
        pass

class reuploadingFourier(torch.nn.Module):
    '''
    Class for the Fourier reuploading, in which the angles are th=1.x+b
    CAVEAT: makes sense only for 1d data?
    '''
    def __init__(self, circuit_name, block, nb_qubits, depth, input_size):
        super(reuploadingFourier, self).__init__() # inherit from torch.nn.Module

        # Check(s) 
        assert(all([gate in oneQubitGates for gate in block.split("_")]))

        # Circuit-related parameters
        self.name = circuit_name
        self.circuit   = circuits.get_circuit(circuit_name)
        self.block = block.split("_")
        self.block_length = len(self.block)
        self.input_size = int(input_size)
        self.nb_qubits = int(nb_qubits)
        self.depth = int(depth)
        #self.seq       = (self.block * self.depth)[:self.depth] #e.g. ['RY', 'RX'] <= define 1 depth as one gate 
        self.seq       = (self.block * self.depth) #e.g. ['RY', 'RX'] # <= define 1 depth as one block

        # Tracker of the circuit evaluations
        self.tracker = Tracker(self.circuit.device)
        
        # Angles and parameters
        self.list_nb_params = self.calc_list_nb_params() # nbr of params for the gates
        self.nb_params = np.sum(self.list_nb_params) #FIXME: valid only if all rotations upload the data
        self.list_nb_angles = self.calc_list_nb_angles()
        self.nb_angles = np.sum(self.list_nb_angles)

        # Parameters initialisation and rescaling
        self.params = torch.rand(self.nb_params, requires_grad=True)
        self.params = self.params.add(-.5).clone().detach().requires_grad_(True) #preferred syntax, cf the docs
        self.params = self.params.multiply(2*torch.pi).clone().detach().requires_grad_(True) 
   
        # Pretty drawing
        draw(self)

    def parameters(self):
        return [self.params]

    # forward func mimicking the MLP one
    def forward(self, train_input, run_infos, module_infos):
        preds = torch.empty((len(train_input))) 
        i = 0 #index
        with Tracker(self.circuit.device) as tracker:
            for inp in train_input:
                angles = calcAnglesFourier(inp, self)
                val_pred = self.circuit(self, angles)
                preds[i] = val_pred
                i += 1
        update_tracker(self.tracker, tracker)
        return preds

    def info(self):
        return  { "nb_params" : self.nb_params,
                "tracker totals": self.tracker.totals,
                "tracker history": self.tracker.history,
                "tracker latest": self.tracker.latest}

    # Functions to assign gate <- nb angles <- nb params
    # TODO: replace by PennyLane rotations attributes
    def calc_nb_angles(self, gate_name):
        if gate_name in nbrParamGates.keys(): return nbrParamGates[gate_name]
        else: raise ValueError('Gate name %s not recognised' %(gate_name))

    def calc_list_nb_angles(self):
        return [self.calc_nb_angles(gate) for gate in self.seq]
        
    #def calc_list_nb_params(self): # 1 bias per angle, no variational params for weights
    #    return [self.calc_nb_angles(gate) for gate in self.seq]
    
    def calc_list_nb_params(self):
        nb = []
        for i in range(len(self.seq)):
            if (i+1) % self.block_length != 0:
                gate = self.seq[i]
                nb.append(self.calc_nb_angles(gate))
        return nb

    def save_model(self, path):
        path += '.csv'
        P = [p.detach() for p in self.params]
        dfParams = pd.DataFrame({'Params': P})
        dfParams.to_csv(path, index=False)

    def load_model(self,path):
        pass

class reuploadingFastFourier(torch.nn.Module):
    '''
    Class for the Fourier reuploading, in which the angles are th=1.x+b
    CAVEAT: makes sense only for 1d data?
    '''
    def __init__(self, circuit_name, block, nb_qubits, depth, input_size):
        super(reuploadingFastFourier, self).__init__() # inherit from torch.nn.Module

        # Check(s) 
        assert(all([gate in oneQubitGates for gate in block.split("_")]))

        # Circuit-related parameters
        self.name = circuit_name
        self.circuit   = circuits.get_circuit(circuit_name)
        self.block = block.split("_")
        self.input_size = int(input_size)
        self.nb_qubits = int(nb_qubits)
        self.depth = int(depth)
        #self.seq       = (self.block * self.depth)[:self.depth] #e.g. ['RY', 'RX'] <= define 1 depth as one gate 
        self.seq       = (self.block * self.depth) #e.g. ['RY', 'RX'] # <= define 1 depth as one block

        # Tracker of the circuit evaluations
        self.tracker = Tracker(self.circuit.device)
        
        # Angles and parameters
        self.list_nb_params = self.calc_list_nb_params() # nbr of params for the gates
        self.nb_params = np.sum(self.list_nb_params) #FIXME: valid only if all rotations upload the data
        self.list_nb_angles = self.calc_list_nb_angles()
        self.nb_angles = np.sum(self.list_nb_angles)

        # Parameters initialisation and rescaling
        self.params = torch.rand(self.nb_params, requires_grad=True)
        self.params = self.params.add(-.5).clone().detach().requires_grad_(True) #preferred syntax, cf the docs
        self.params = self.params.multiply(2*torch.pi).clone().detach().requires_grad_(True) 
   
        # Pretty drawing
        draw(self)

    def parameters(self):
        return [self.params]

    # forward func mimicking the MLP one
    def forward(self, train_input, run_infos, module_infos):
        preds = torch.empty((len(train_input))) 
        i = 0 #index
        with Tracker(self.circuit.device) as tracker:
            for inp in train_input:
                angles = calcAnglesFourier(inp, self)
                val_pred = self.circuit(self, angles)
                preds[i] = val_pred
                i += 1
        update_tracker(self.tracker, tracker)
        return preds

    def info(self):
        return  { "nb_params" : self.nb_params,
                "tracker totals": self.tracker.totals,
                "tracker history": self.tracker.history,
                "tracker latest": self.tracker.latest}

    # Functions to assign gate <- nb angles <- nb params
    # TODO: replace by PennyLane rotations attributes
    def calc_nb_angles(self, gate_name):
        if gate_name in nbrParamGates.keys(): return nbrParamGates[gate_name]
        else: raise ValueError('Gate name %s not recognised' %(gate_name))

    def calc_list_nb_angles(self):
        return [self.calc_nb_angles(gate) for gate in self.seq]
        
    def calc_list_nb_params(self): # 1 bias per angle, no variational params for weights
        return [self.calc_nb_angles(gate) for gate in self.seq]
    

    def save_model(self, path):
        path += '.csv'
        P = [p.detach() for p in self.params]
        dfParams = pd.DataFrame({'Params': P})
        dfParams.to_csv(path, index=False)

    def load_model(self,path):
        pass


class reuploadingZYZ(torch.nn.Module):
    '''
    Class for the full reuploading, in which the angles are
    parametrised as in https://arxiv.org/abs/2205.07848, i.e.
    one block is RZ(phi)RY(th)RZ(x)
    '''
    def __init__(self, circuit_name, block, nb_qubits, depth, input_size):
        super(reuploadingZYZ, self).__init__() # inherit from torch.nn.Module

        # Check(s) 
        assert(all([gate in oneQubitGates for gate in block.split("_")]))

        # Circuit-related parameters
        self.name = circuit_name
        self.circuit   = circuits.get_circuit(circuit_name)
        self.block = block.split("_")
        self.block_length = len(self.block)
        self.input_size = int(input_size)
        self.nb_qubits = int(nb_qubits)
        self.depth = int(depth)
        #self.seq       = (self.block * self.depth)[:self.depth] #e.g. ['RY', 'RX'] <= define 1 depth as one gate 
        self.seq       = (self.block * self.depth) #e.g. ['RY', 'RX'] # <= define 1 depth as one block

        # Tracker of the circuit evaluations
        self.tracker = Tracker(self.circuit.device)

        # Angles and parameters
        self.list_nb_params = self.calc_list_nb_params() # nbr of params for the gates
        self.nb_params = np.sum(self.list_nb_params)
        self.list_nb_angles = self.calc_list_nb_angles()
        self.nb_angles = np.sum(self.list_nb_angles)

        # Parameters initialisation and rescaling
        self.params = torch.rand(self.nb_params, requires_grad=True)
        self.params = self.params.add(-.5).clone().detach().requires_grad_(True) #preferred syntax, cf the docs
        self.params = self.params.multiply(2*torch.pi).clone().detach().requires_grad_(True) 
        
        # Pretty drawing
        draw(self)

    def parameters(self):
        return [self.params]

    # forward func mimicking the MLP one
    def forward(self, train_input, run_infos, module_infos):
        preds = torch.empty((len(train_input))) 
        i = 0 #index
        #with Tracker(self.circuit.device, callback=self.update_tracker) as tracker:
        with Tracker(self.circuit.device) as tracker:
            #with self.tracker as tracker:
            for inp in train_input:
                #angles = calcAnglesZYZ(inp, self)
                angles = calcAngleFourier(inp, self)
                val_pred = self.circuit(self, angles)
                preds[i] = val_pred
                i += 1
        update_tracker(self.tracker, tracker)
        print(self.tracker.totals)
        return preds

    def info(self):
        return  { "nb_params" : self.nb_params,
                "tracker totals": self.tracker.totals,
                "tracker history": self.tracker.history,
                "tracker latest": self.tracker.latest}

    # Functions to assign gate <- nb angles <- nb params
    # TODO: replace by PennyLane rotations attributes
    def calc_nb_angles(self, gate_name):
        if gate_name in nbrParamGates.keys(): return nbrParamGates[gate_name]
        else: raise ValueError('Gate name %s not recognised' %(gate_name))

    def calc_list_nb_angles(self):
        return [self.calc_nb_angles(gate) for gate in self.seq]
        
    def calc_list_nb_params(self):
        nb = []
        for i in range(len(self.seq)):
            if (i+1) % self.block_length != 0:
                gate = self.seq[i]
                nb.append(self.calc_nb_angles(gate))
        return nb
    
    def save_model(self, path):
        path += '.csv'
        P = [p.detach() for p in self.params]
        dfParams = pd.DataFrame({'Params': P})
        dfParams.to_csv(path, index=False)

    def load_model(self,path):
        pass



# Classes for TTN and MERA.
# TODO: write a base class, as done for single qubit
class ttn_and_mera_fixed(torch.nn.Module):
    def __init__(self, circuit_name, block, nb_qubits, input_size, downscaling):
        super(ttn_and_mera_fixed, self).__init__() # inherit from torch.nn.Module

        # Circuit-related parameters
        self.name = "TTN"
        self.circuit = circuits.get_circuit(self.name)
        self.block = block.split("_")
        self.downscaling = 2 # TODO improve this #int(downscaling)
        self.input_size = int(input_size)
        #self.nb_qubits = self.input_size # not what we want here... this is more a kernel classifier thing
        self.nb_qubits = int(nb_qubits)
        
        self.depth = int(np.log(self.nb_qubits) / np.log(2))
        self.nb_blocks = int( self.nb_qubits * (1 / self.downscaling - (1 / self.downscaling)**(self.depth + 1)) / (1 - 1 / self.downscaling) )
        self.seq       = (self.block * self.depth) #e.g. ['RY', 'RX'] # <= define 1 depth as one block
        self.layers    = [self.block] * self.depth # a bit more convenient for looping, see TTN circuit

        # Angles and parameters
        self.list_nb_params = self.calc_list_nb_params() # nbr of params for the gates
        self.nb_params = np.sum(self.list_nb_params)
        self.list_nb_angles = self.calc_list_nb_angles()
        self.nb_angles = np.sum(self.list_nb_angles)

        # Tracker of the circuit evaluations
        self.tracker = Tracker(self.circuit.device)
        
        # Parameters initialisation and rescaling
        self.params = torch.rand(self.nb_params, requires_grad=True)
        self.params = self.params.add(-.5).clone().detach().requires_grad_(True) #preferred syntax, cf the docs
        self.params = self.params.multiply(2*torch.pi).clone().detach().requires_grad_(True) 
        
        # Pretty drawing # TODO: fix this, although it's kind of useless
        #draw(self)
   
    def parameters(self):
        return [self.params]

    # forward func mimicking the MLP one
    def forward(self, train_input, run_infos, module_infos):
        preds = torch.empty((len(train_input))) 
        i = 0 #index
        with Tracker(self.circuit.device) as tracker:
            for inp in train_input:
                angles = calcAnglesRuc(inp, self)
                val_pred = self.circuit(self, angles)
                preds[i] = val_pred
                i += 1
        update_tracker(self.tracker, tracker)
        return preds
    
    # Functions to assign gate <- nb angles <- nb params
    # TODO: replace by PennyLane rotations attributes
    def calc_list_nb_params(self): 
        #TODO: this should be able to consider the reuploading scheme, e.g. R(wx+b)R(phi)
        # here I assume th=w.x+b for all
        return [(self.input_size + 1) * self.calc_nb_angles(gate) for gate in self.block] * self.nb_blocks * self.downscaling
    
    def calc_list_nb_angles(self):
        return [self.calc_nb_angles(gate) for gate in self.block] * self.nb_blocks * self.downscaling
    
    def calc_nb_angles(self, gate_name):
        if gate_name in nbrParamGates.keys(): return nbrParamGates[gate_name]
        else: raise ValueError('Gate name %s not recognised' %s)
        
    def info(self):
        return  { "nb_params" : self.nb_params,
                "tracker totals": self.tracker.totals,
                "tracker history": self.tracker.history,
                "tracker latest": self.tracker.latest}

    def save_model(self, path):
        path += '.csv'
        P = [p.detach() for p in self.params]
        dfParams = pd.DataFrame({'Params': P})
        dfParams.to_csv(path, index=False)
        # dump results here, but should be done in a cleaner way

    def load_model(self,path):
        pass

class ttn_and_mera_linear(torch.nn.Module):
    def __init__(self, circuit_name, block, input_size, encoding, downscaling, nb_qubits):
        super(ttn_and_mera_linear, self).__init__() # inherit from torch.nn.Module
        
        # Circuit-related parameters
        self.name = "TTN"
        self.circuit = circuits.get_circuit(self.name)
        self.block = block.split("_")
        self.downscaling = int(downscaling)
        self.input_size = int(nb_qubits)
        self.nb_qubits = self.input_size

        self.depth = int(np.log(self.nb_qubits) / np.log(2))
        self.nb_blocks = int( self.nb_qubits * (1 / self.downscaling - (1 / self.downscaling)**(self.depth + 1)) / (1 - 1 / self.downscaling) )
        self.seq       = (self.block * self.depth) #e.g. ['RY', 'RX'] # <= define 1 depth as one block
        self.layers    = [self.block] * self.depth # a bit more convenient for looping, see TTN circuit
        
        # Angles and parameters
        self.list_nb_params = self.calc_list_nb_params() # nbr of params for the gates
        self.nb_params = np.sum(self.list_nb_params)
        self.list_nb_angles = self.calc_list_nb_angles()
        self.nb_angles = np.sum(self.list_nb_angles)
        
        # Tracker of the circuit evaluations
        self.tracker = Tracker(self.circuit.device)
        print(f'depth {self.depth}') 
        print(f'nb blocks {self.nb_blocks}') 
        print(f'block {self.block}') 
        print(f'nb params {self.nb_params}') 
        # Parameters initialisation and rescaling
        self.params = torch.rand(self.nb_params, requires_grad=True)
        self.params = self.params.add(-.5).clone().detach().requires_grad_(True) #preferred syntax, cf the docs
        self.params = self.params.multiply(2*torch.pi).clone().detach().requires_grad_(True) 
        
        # Pretty drawing
        draw(self)
   
    def parameters(self):
        return [self.params]

    # forward func mimicking the MLP one
    def forward(self, train_input, run_infos, module_infos):
        preds = torch.empty((len(train_input))) 
        i = 0 #index
        with Tracker(self.circuit.device) as tracker:
            for inp in train_input:
                encoding_angles = inp
                val_pred = self.circuit(self, self.params, encoding_angles)
                preds[i] = val_pred
                i += 1
        update_tracker(self.tracker, tracker)
        return preds
    
    # Functions to assign gate <- nb angles <- nb params
    def calc_nb_angles(self, gate_name):
        if gate_name in nbrParamGates.keys(): return nbrParamGates[gate_name]
        else: raise ValueError('Gate name %s not recognised' %s)

    def calc_list_nb_angles(self):
        return [self.calc_nb_angles(gate) for gate in self.block] * self.nb_blocks * self.downscaling
        
    def calc_list_nb_params(self): #TODO: this should be able to consider the reuploading scheme, e.g. R(wx+b)R(phi)
        return [self.calc_nb_angles(gate) for gate in self.block] * self.nb_blocks * self.downscaling
    
    def info(self):
        return  { "nb_params" : self.nb_params,
                "tracker totals": self.tracker.totals,
                "tracker history": self.tracker.history,
                "tracker latest": self.tracker.latest}

    def save_model(self, path):
        path += '.csv'
        P = [p.detach() for p in self.params]
        dfParams = pd.DataFrame({'Params': P})
        dfParams.to_csv(path, index=False)

    def load_model(self,path):
        pass
