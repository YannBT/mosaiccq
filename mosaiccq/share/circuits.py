import pennylane as qml
#from torch import dot, FloatTensor
#from torch import tensor, Tensor
import convenience_qnn as convq

dev = qml.device('default.qubit', wires=10) # CAVEAT: wires must be equal to or larger than the max number of planned qubits
#FIXME: the runtime depends on the nbr of wires, is there a way of setting it properly?
dev1 = qml.device('default.qubit', wires=1) # to ensure RUC are fast
#dev2 = qml.device('default.qubit', wires=2)
#dev4 = qml.device('default.qubit', wires=4)

# TODO: at some point, change the name RUC -> MQC = 'mono-qubit circuit'

@qml.qnode(dev1, interface='torch')
def RUC(model, angles):
    '''
    Apply rotations according to a reuploading circuit (RUC) architecture, i.e. input-dependent mono-qubit rotations.
    '''
    # Convenience defs
    seq = model.seq
    s = 1
    tracked = list(range(model.nb_qubits)) # tracked wire, here for consistency wrt apply_gate()
    nb_layers = len(model.seq)
    idx = 0 #nbr of exchausted angles
    
    # Operations
    for l in range(nb_layers):
        gate_name = seq[l]
        idx = apply_gate(gate_name, angles, tracked, idx)

    return qml.expval(qml.PauliZ(0))

@qml.qnode(dev, interface='torch')
def TTN(model, angles, encoding_angles=None): #s: downscaling factor
    '''
    Apply rotations according to a tree tensor network (TTN) architecture, with downscaling factor s.
    '''

    # Convenience defs
    seq = model.seq
    block = model.block
    tracked = list(range(model.nb_qubits)) #tracked wires
    layers = len(model.seq) #TODO: not always true
    idx = 0 #nbr of exchausted angles

    # Angle encoding
    if encoding_angles is not None:
        [qml.RY(angle, wires=w) for angle, w in zip(encoding_angles, range(len(encoding_angles)))]
    
    # Operations
    for layer in model.layers:
        # TODO: gates on a slice are all the same, but there can be more than 1 gate per layer
        # loop over depth, apply gates in block, return tracked and idx
        # this will prob reauire rewriting the apply_layer() function
        for gate_name in layer:
            idx = apply_gate(gate_name, angles, tracked, idx, model.downscaling)
        tracked = tracked[::model.downscaling]
    assert(tracked == [0]) # see below
    return qml.expval(qml.PauliZ(0)) #NOTE personal conv: ALWAYS manage to measure qubit zero

@qml.qnode(dev, interface='torch')
def MERA(model, angles, encoding_angles): #s: downscaling factor
    '''
    Apply rotations according to a multiscale entanglement renormalisation ansatz (MERA) architecture, with downscaling factor s.
    '''

    # Convenience defs
    seq = model.seq
    s = model.downscaling
    tracked1 = list(range(model.nb_qubits))   #tracked wires
    tracked2 = list(range(1,model.nb_qubits)) #tracked wires for the extra layers
    layers = len(model.seq)
    idx = 0 #nbr of exchausted angles

    # Angle encoding
    if encoding_angles is not None:
        [qml.RY(angle, wires=w) for angle, w in zip(encoding_angles, range(len(encoding_angles)))]
    
    # Operations
    for l in range(layers):
        gate_name = seq[l]
        tracked1, idx = apply_gate(gate_name, angles, tracked1, idx)
        
        if convq.nqubits[gate_name] > 1:
            tracked2, idx = apply_gate(gate_name, angles, tracked2, idx)

    assert(tracked == [0]) # see below
    return qml.expval(qml.PauliZ(0)) #NOTE personal conv: ALWAYS manage to measure qubit zero


### Convenience function(s)
def apply_gate(gate_name, angles, tracked, idx, downscaling=None, PBC=False): 
    '''
    Generic function for applying a gate and returning the parameters and qubits indices.
    If PBC=True, periodic boundary conditions are applied.
    '''
    rot = convq.getRot(gate_name)
    # mono-qubit rotation
    if convq.nqubits[gate_name] == 1:
        for qb in tracked:
            if gate_name in convq.PL0p:
                rot(wires=qb)
            elif gate_name in convq.PL1p:
                rot(angles[idx], wires=qb)
                idx += 1
            elif gate_name in convq.PL2p:
                rot(angles[idx], angles[idx+1], wires=qb)
                idx += 2
            elif gate_name in convq.PL3p:
                rot(angles[idx], angles[idx+1], angles[idx+2], wires=qb)
                idx += 3
    # multiple-qubit rotation
    else:
        for ctrl, tgt in zip(tracked[::downscaling], tracked[1::downscaling]):
            if gate_name in convq.PL0p:
                rot(wires=[ctrl, tgt])
            elif gate_name in convq.PL1p:
                rot(angles[idx], wires=[ctrl, tgt])
                idx += 1
            elif gate_name in convq.PL2p:
                rot(angles[idx], angles[idx+1], wires=[ctrl, tgt])
                idx += 2
            elif gate_name in convq.PL3p:
                rot(angles[idx], angles[idx+1], angles[idx+2], wires=[ctrl, tgt])
                idx += 3
        if PBC:
            raise NotImplementedError()
    return idx

def get_circuit(name):
    '''
    Convenience function for easier calls within the code.
    Basically a glue layer between the "name" string and the corresponding function.
    '''
    if name == 'RUC': return RUC
    elif name == 'TTN2': return TTN2
    elif name == 'TTN': return TTN
    elif name == 'MERA': return MERA
    else:
        raise KeyError('Could not find the circuit for ', name)
