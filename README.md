# Short description
The MOSAICCQ (MOdelS for AI Comparison, Classical & Quantum) is a key-in-hand library build for facilitating research-level studies involving the use of classical and (circuit-based) quantum machine learning models. 
By lack of work forces, this project is currently on hold. 

# Quick install
After having cloned the framework for this repository, you can install all the necessary dependencies and build the code using the following command:
  
> python3 -m build && sudo pip3 install dist/*.whl --force-reinstall

# History tree of this project
The program at the root of this project is the "qml" (for Quantum Machine Learning) code, that I wrote from Feb. to Apr. 2022 within the scope of my post-doc. The qml code was migrated into my institution GitLab and continued therefrom, under the no less dull name "frameworkcodeqml". Around July 2022, the code was refactored in collaboration with Mattéo Papin (École 42, France) and Frédéric Magniette (École Polytechnique, France) into the MOSAIC code. The quantum machine learning version of this code, with several features added, is MOSAICCQ. 


```mermaid
graph 
A(qml) -- to CNRS GitLab --> B(frameworkcodeqml)
B -- back to personal GitLab--> C(MOSAICCQ)
B -- to CNRS-LLR GitLab --> D(MOSAIC)
```

For more information on the MOSAIC project, please visit [this page](https://llrogcid.in2p3.fr/the-mosaic-framework/).
For more information on the MOSAICCQ project, please contact me at yann.beaujeault-t [at] protonmail [.] com

Due to administrative/political reasons, the two branches must be kept separate. 
